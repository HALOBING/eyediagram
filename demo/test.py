from matplotlib import pyplot as plt
import numpy as np

t = np.linspace(-np.pi, np.pi, 200)
ys = [np.random.normal(1, .05) * np.sin(np.random.normal(1, .01) * t + np.random.normal(0, .15))
      for i in range(0, 1000)]
fig, axs = plt.subplots(nrows=3, sharex=True)
axs[0].plot(t, np.array(ys).T)
axs[1].plot(t, np.array(ys).T, color='crimson', alpha=.1, lw=.1)
axs[2].hist2d(np.tile(t, len(ys)), np.ravel(ys), bins=(200, 50), cmap='inferno')
plt.show()

