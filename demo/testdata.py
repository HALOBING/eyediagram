from eyediagram.demo_data import demo_data
from eyediagram.core import grid_count as _grid_count
num_symbols = 5000
samples_per_symbol = 24
y = demo_data(num_symbols, samples_per_symbol)
counts = _grid_count(y, 2*samples_per_symbol, 16)

f = open('./eyediagram.json', mode='w+')
f.writelines(str(counts.tolist()))
f.flush()
f.close()


